import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np

sns.set_theme()
BASE_AMOUNT = 1
NUMBER_OF_BUYERS = 20
AVR_FLUIDITY = 7
STDV_FLUIDITY = 3
MAX_PRICE_FACTOR = 3
NUM_OF_ITERS = 200
BASE_PRICE = 40
PRICE_EXP = 1.005
PRICE_OF_PROD = 40
HUNGER_RAISE = 3

HISTORY = [[]]

class Merchent:
    def __init__(self):
        self.history = []
        self.amount_prod = 0
        self.price = 0

    def action(self, price):
        self.price = BASE_PRICE + price
        HISTORY[0].append(price)
class Buyer():
    def __init__(self, max_price,fluid, amount = BASE_AMOUNT):
        self.max_price = max_price
        self.amount = amount
        self.current = max_price
        self.start_fluid = fluid//2
        self.fluid = self.start_fluid
        self.hungry = 0

    def do_i_buy(self, offer):
        if offer <= self.current + self.fluid and offer <= self.max_price:
            self.current = (offer + self.current)//2
            self.fluid = self.start_fluid
            return self.amount
        else:
            self.hungry += 1
            if not self.hungry%HUNGER_RAISE:
                self.fluid += 1

class MarketEnv:
    def __init__(self, number_of_merchents, list_of_max_prices, list_of_fluidity, list_of_demand = [BASE_AMOUNT]*NUMBER_OF_BUYERS):
        self.ACTION_SPACE_SIZE = 30
        self.OBSERVATION_SPACE_VALUES = (number_of_merchents,) # number of buyers, prices of other agents in previous turn
        self.log_of_prices = np.array([np.array([0]*number_of_merchents)])
        self.counter = 0
        self.buyers = [0 for _ in range(len(list_of_demand))]
        for i in range(len(list_of_demand)):
            self.buyers[i] = Buyer(list_of_max_prices[i], list_of_fluidity[i])
        self.num_of_merchents = number_of_merchents

    def reset(self):
        self.merchents = [0 for _ in range(self.num_of_merchents)]
        for i in range(self.num_of_merchents):
            self.merchents[i] = Merchent()
        self.episode_step = 0
        # HISTORY[0] = []
        self.counter = 0
        observation = np.random.randint(0,self.ACTION_SPACE_SIZE, size = self.OBSERVATION_SPACE_VALUES)
        return observation

    def step(self, actions):
        #reward = [-(actions[i][1] * PRICE_OF_PROD) ** PRICE_EXP - BASE_PRICE for i in range(len(actions))]
        reward = [0 for _ in range(len(actions))]
        amount_sold = [0 for _ in range(len(actions))]
        for i, player in enumerate(self.merchents):
            player.action(actions[i])
        for buyer in self.buyers:
            seller = np.random.randint(self.num_of_merchents)
            amount = buyer.do_i_buy(self.merchents[seller].price)
            if amount:
                reward[seller] += amount * self.merchents[seller].price
                amount_sold[seller] += 1
        done = False
        for i in range(self.num_of_merchents):
            reward[i] -= (amount_sold[i] * PRICE_OF_PROD) ** PRICE_EXP + BASE_PRICE
        self.counter+=1
        if  self.counter >= NUM_OF_ITERS:
            self.counter = 0
            done = True
        new_observation = np.array(actions)
        return new_observation, reward, done

    def render(self):
        plt.xlabel("iteration")
        plt.ylabel("price")
        for i,seller in enumerate(self.merchents):
            # if (len(HISTORY) > 500):
            #     plt.scatter(range(len(HISTORY[-500:-1])), HISTORY[-500:-1], label='merchent %s' % i, s = 3)
            # else:
            plt.scatter(range(len(HISTORY[0])), HISTORY[0], label='merchent %s' % i, s = 0.1)
        plt.legend()
        plt.show()

def create_buyers(number, avarage_fluidity = AVR_FLUIDITY,stdv = STDV_FLUIDITY):
    fluidity = np.abs(np.round(np.random.normal(avarage_fluidity, stdv,size = number)))
    max_price = BASE_PRICE + MAX_PRICE_FACTOR*fluidity
    return fluidity, max_price

# fluidity, max_price = create_buyers(100)
# market = MarketEnv(2, max_price, fluidity)
# market.reset()
# for i in range(10):
#     print(market.step([50,60]))
# market.render()

