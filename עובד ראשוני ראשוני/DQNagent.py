import numpy as np
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
from datetime import datetime
import tensorflow as tf
import tensorflow_datasets as tfds
import threading
from market import *
import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout, Conv2D, MaxPooling2D, Activation, Flatten
from keras.optimizers import Adam
from keras.callbacks import TensorBoard
from collections import deque
import time
import random
from tqdm import tqdm
import numpy as np

AGENT_NUMBER = 0
MODEL_FLUIDNESS = 4

I_WANT_TO_PLOT = [0]
I_WANT_TO_SAVE = [0]
DISCOUNT = 0.99
REPLAY_MEMORY_SIZE = 50_000  # How many last steps to keep for model training
MIN_REPLAY_MEMORY_SIZE = 1_000  # Minimum number of steps in a memory to start training
MINIBATCH_SIZE = 64  # How many steps (samples) to use for training
UPDATE_TARGET_EVERY = 5  # Terminal states (end of episodes)
MODEL_NAME = '2x256'
MIN_REWARD = -200  # For model save
MEMORY_FRACTION = 0.20

# Environment settings
EPISODES = 20_000

# Exploration settings
epsilon = 1  # not a constant, going to be decayed
EPSILON_DECAY = 0.9
MIN_EPSILON = 0.01

MIN_NEURONS = (14,)

#  Stats settings
AGGREGATE_STATS_EVERY = 50  # episodes
SHOW_PREVIEW = True
from market import NUMBER_OF_BUYERS
fluidity, max_price = create_buyers(NUMBER_OF_BUYERS)
env = MarketEnv(1, max_price, fluidity)
now = datetime.now()

def do_you_want_to_plot():
    a = input("do you want to plot or save?")
    if a == "p":
        I_WANT_TO_PLOT[0] = 1
        return
    elif a == "s":
        I_WANT_TO_SAVE[0] = 1
        return
    else:
        do_you_want_to_plot()
        return


th = threading.Thread(target=do_you_want_to_plot)


# For stats
ep_rewards = [-200]

# For more repetitive results
random.seed(1)
np.random.seed(1)
tf.random.set_seed(1)

# Memory fraction, used mostly when trai8ning multiple agents
#gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=MEMORY_FRACTION)
#backend.set_session(tf.Session(config=tf.ConfigProto(gpu_options=gpu_options)))

# Create models folder
if not os.path.isdir('models'):
    os.makedirs('models')

# Agent class
class DQNAgent:
    def __init__(self):

        # Main model
        # self.model = keras.models.load_model('models/2020-12-29-21.48.32__19200.63avg_.model')
        self.model = self.create_model()

        # Target network
        self.target_model = self.create_model()
        self.target_model.set_weights(self.model.get_weights())

        # An array with last n steps for training
        self.replay_memory = deque(maxlen=REPLAY_MEMORY_SIZE)


        # Used to count when to update target network with main network's weights
        self.target_update_counter = 0

    def create_model(self):
        model = Sequential()

        model.add(Activation('relu', input_shape=env.OBSERVATION_SPACE_VALUES))
        model.add(tf.keras.layers.Dense(30, activation= "relu"))
        model.add(Activation('relu'))
        model.add(Dropout(0.2))

        model.add(Activation('relu'))
        model.add(Activation('relu'))
        model.add(Activation('relu'))
        model.add(Dropout(0.2))

        model.add(Flatten())  # this converts our 3D feature maps to 1D feature vectors
        model.add(Dense(64))

        model.add(Dense(env.ACTION_SPACE_SIZE, activation='linear'))  # ACTION_SPACE_SIZE = how many choices (30 prices)
        model.compile(loss="mse", optimizer=Adam(lr=0.001), metrics=['accuracy'])
        print(model.summary())
        return model

    # Adds step's data to a memory replay array
    # (observation space, action, reward, new observation space, done)
    def update_replay_memory(self, transition):
        self.replay_memory.append(transition)

    # Trains main network every step during episode
    def train(self, terminal_state, step):

        # Start training only if certain number of samples is already saved
        if len(self.replay_memory) < MIN_REPLAY_MEMORY_SIZE:
            return

        # Get a minibatch of random samples from memory replay table
        minibatch = random.sample(self.replay_memory, MINIBATCH_SIZE)

        # Get current states from minibatch, then query NN model for Q values
        current_states = np.array([transition[0] for transition in minibatch])/(BASE_PRICE + env.ACTION_SPACE_SIZE)
        current_qs_list = self.model.predict(current_states)

        # Get future states from minibatch, then query NN model for Q values
        # When using target network, query it, otherwise main network should be queried
        new_current_states = np.array([transition[3] for transition in minibatch])/(BASE_PRICE + env.ACTION_SPACE_SIZE)
        future_qs_list = self.target_model.predict(new_current_states)

        X = []
        y = []

        # Now we need to enumerate our batches
        for index, (current_state, action, reward, new_current_state, done) in enumerate(minibatch):

            # If not a terminal state, get new q from future states, otherwise set it to 0
            # almost like with Q Learning, but we use just part of equation here
            if not done:
                max_future_q = np.max(future_qs_list[index])
                new_q = reward + DISCOUNT * max_future_q
            else:
                new_q = reward

            # Update Q value for given state
            current_qs = current_qs_list[index]
            current_qs[action] = new_q

            # And append to our training data
            X.append(current_state)
            y.append(current_qs)

        # Fit on all samples as one batch, log only on terminal state
        self.model.fit(np.array(X)/(BASE_PRICE + env.ACTION_SPACE_SIZE), np.array(y), batch_size=MINIBATCH_SIZE, verbose=0, shuffle=False,)

        # Update target network counter every episode
        if terminal_state:
            self.target_update_counter += 1

        # If counter reaches set value, update target network with weights of main network
        if self.target_update_counter > UPDATE_TARGET_EVERY:
            self.target_model.set_weights(self.model.get_weights())
            self.target_update_counter = 0

    # Queries main network for Q values given current observation space (environment state)
    def get_qs(self, state):
        return self.model.predict(np.array(state).reshape(-1, *state.shape)/(BASE_PRICE + env.ACTION_SPACE_SIZE))[0]

agent = DQNAgent()
th.start()
action = np.round(env.ACTION_SPACE_SIZE/2)
# Iterate over episodes
for episode in range(1, EPISODES + 1):

    # Update tensorboard step every episode
    # agent.tensorboard.step = episode

    # Restarting episode - reset episode reward and step number
    episode_reward = 0
    step = 1

    # Reset environment and get initial state
    current_state = env.reset()

    # Reset flag and start iterating until episode ends
    done = False
    while not done:

        # This part stays mostly the same, the change is to query a model for Q values
        if np.random.random() > epsilon:
            # Get action from Q table
            action = np.argmax(agent.get_qs(current_state))
        else:
            # Get random action
            action = int(np.round(np.random.normal(action, MODEL_FLUIDNESS)))
            if action <= 0:
                action = 0
            elif action >= env.ACTION_SPACE_SIZE -1:
                action = env.ACTION_SPACE_SIZE -1

        new_state, reward, done = env.step([action])

        # Transform new continous state to new discrete state and count reward
        episode_reward += reward[AGENT_NUMBER]

        # if SHOW_PREVIEW and not episode % AGGREGATE_STATS_EVERY:

        # Every step we update replay memory and train main network
        agent.update_replay_memory((current_state, action, reward[AGENT_NUMBER], new_state, done))
        agent.train(done, step)

        current_state = new_state
        step += 1

        if I_WANT_TO_PLOT[0]:
            env.render()
            I_WANT_TO_PLOT[0] = 0
            th = threading.Thread(target=do_you_want_to_plot)
            th.start()
    # Append episode reward to a list and log stats (every given number of episodes)
    ep_rewards.append(episode_reward)
    if not episode % AGGREGATE_STATS_EVERY or episode == 1 or I_WANT_TO_SAVE[0]:
        if I_WANT_TO_SAVE[0]:
            I_WANT_TO_SAVE[0] = 0
            th = threading.Thread(target=do_you_want_to_plot)
            th.start()
        average_reward = sum(ep_rewards[-AGGREGATE_STATS_EVERY:])/len(ep_rewards[-AGGREGATE_STATS_EVERY:])
        min_reward = min(ep_rewards[-AGGREGATE_STATS_EVERY:])
        max_reward = max(ep_rewards[-AGGREGATE_STATS_EVERY:])
        # agent.tensorboard.update_stats(reward_avg=average_reward, reward_min=min_reward, reward_max=max_reward, epsilon=epsilon)

        # Save model, but only when min reward is greater or equal a set value
        if min_reward >= MIN_REWARD:
            agent.model.save(f'models/{now.strftime("%Y-%m-%d-%H.%M.%S")}__{average_reward:_>7.2f}avg_.model')
            now = datetime.now()

    # Decay epsilon
    if epsilon > MIN_EPSILON:
        epsilon *= EPSILON_DECAY
        epsilon = max(MIN_EPSILON, epsilon)